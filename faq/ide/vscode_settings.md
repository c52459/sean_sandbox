vscode defines to that the preprocessor does not exclude all code !!!

these are my VSCODE user settings:

extensions
==========

c/c++ (ms-vscode.cpptools?)
plantuml viewer (for markdown preview of plantuml) (jebbs.plantuml?)

settings
========

to get intellisense working properly
------------------------------------

C:\Users\username\AppData\Roaming\Code\User\c_cpp_properties.json

```json
{
    "defines":[
        "MSCC_COMEXPRESS",
        "PM6000",
        "PM6010",
        "PM6017",
        "DIGI_G5_P_B_RELEASE=24",
        "_WIN32",
        "PUBLIC= ",
        "PRIVATE=static"
    ]
}
```

user settings.json
------------------

C:\Users\username\AppData\Roaming\Code\User\settings.json

```json
{
"files.associations": {
    "*.c":"cpp",
    "*.h":"cpp"
},
"plantuml.jar": "c:\\utils\\plantuml.jar",
"plantuml.fileExtensions": ".wsd,.pu,.puml,.plantuml,.iuml,.md",
"plantuml.render": "Local",
"plantuml.server": "https://www.plantuml.com/plantuml",
"window.zoomLevel": 0,
"files.exclude": {
    "*.d": true,
    "*.o": true,
    "**/builds": true,
    "**/doxygen_output": true,
    "**/html": true,
    "**/log_examples": true,
    "**/log_unit_tests": true
}
}
```

### because our .c files are actually c++

```json
"files.associations": {
    "*.c":"cpp",
    "*.h":"cpp"
}
```

### for the markdown viewer plugin,... need to specify where plantuml.jar is:

```json
"plantuml.jar": "c:\\utils\\plantuml.jar",
"plantuml.fileExtensions": ".wsd,.pu,.puml,.plantuml,.iuml,.md",
"plantuml.render": "Local",
"plantuml.server": "https://www.plantuml.com/plantuml",
```

### so that when you open a directory it omits the build directories/files

```json
"files.exclude": {
    "*.d": true,
    "*.o": true,
    "**/builds": true,
    "**/doxygen_output": true,
    "**/html": true,
    "**/log_examples": true,
    "**/log_unit_tests": true
}
```