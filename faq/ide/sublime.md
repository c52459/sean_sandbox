
proper syntax highlighting
==========================
To make .c files think they are c++:
- in bottom status bar, the 'language' is displayed as 'c'  (just after Spaces: 4), 
- left click on the 'language' indicator that says 'c'.
- a popup window shows all available languages, and at the top of the popup window is another option called "open all with current extension as ...".
- select "open all with current extension as ...", then "c++"
- the bottom status bar should now show 'c++' instead of 'c'

cool features
=============

## multi-select

when shift-right-click isn't good enough for multi-select

if you want to change:

        hello there
          hello there

        hello there
to:

        hi there
          hi there

        hi there

- select the first "hello",
- then hit ctrl-d twice,
- now you have three "hello"s selected, and three cursors active,... whatever you type goes into those three spots.
- type 'hi'.

(great way to multi-select)

note: hit ctrl-u to undo last ctrl-d

# goto-any-file
    goto-any-file is ctrl-p
      e.g. to go to mscc_server_socket.c, I type ctrl-p, 'server_sock', enter.

# goto function
    goto-function in file is ctrl-r

# goto function in project
    goto-function in project is ctrl-shift-r

# reveal in sidebar
    slickedit automatically updates the sidebar to include the currently selected window, but in sublime you need to:

    use right-click -> 'reveal in sidebar', to align the folder tab with the current window)

user preferences
================
My user preferences (important is the folder_exclude_patterns and "remember_open_files": false  (after you close all sublime windows and then open sublime again it seems to open a bunch more windows)


```json
{
    "color_scheme": "Packages/Monokai Extended/Monokai Extended.tmTheme",
    "default_line_ending": "unix",
    "file_exclude_patterns":
    [
        "*.pyc",
        "*.pyo",
        "*.exe",
        "*.dll",
        "*.obj",
        "*.o",
        "*.a",
        "*.lib",
        "*.so",
        "*.dylib",
        "*.ncb",
        "*.sdf",
        "*.suo",
        "*.pdb",
        "*.idb",
        ".DS_Store",
        "*.class",
        "*.psd",
        "*.db",
        "*.sublime-workspace",
        "*.o"
    ],
    "folder_exclude_patterns":
    [
        ".svn",
        ".git",
        ".hg",
        ".vs",
        ".vscode",
        "CVS",
        "builds",
        "doxygen_output",
        "visualstudio",
        "doxygen_output",
        "log*",
        "releases",
        "html",
        "latex",
        "g5_vs",
        "sn",
        "x64",
        "logdir",
        "replays",
        "archive"
    ],
    "font_size": 14,
    "format": "png",
    "hot_exit": false,
    "ignored_packages":
    [
        "Vintage"
    ],
    "remember_open_files": false,
    "show_errors_inline": false,
    "show_full_path": false,
    "theme": "Default.sublime-theme",
    "viewer": "WindowsDefaultViewer"
}
```

project file
============
you can simply point sublime to a folder and it does a decent job of tagging everything,
but you'd have to have specified folder excludes to avoid reading the 'builds' directory and everything you do not want the editor to index.

instead, i now use the following project file rather than simply opening a directory.

I copy this project file into each new svn checkout of g5

project.sublime-project 

```json
{
    "folders":
    [
        {
            "path": ".",
            "file_include_patterns": ["*.mk", "makefile", "README*", "project*"],
            "folder_include_patterns": ["none"]
        },
        {
            "path": "device"
        },
        {
            "path": "host"
        },
        {
            "path": "scripts"
        },
        {
            "path": "examples"
        },
        {
            "path": "tests"
        },
        {
            "path": "faq"
        },
        {
            "path": "H:\\notes"
        },
    ]
}
```

Goto next error (workflow for compiler errors)
==============================================

I build on linux and pipe the output through 'sublime.sh'

    make -j10 | sublime.sh

Where sublime.sh is /home/wattbyro/bin/sublime.sh

And I have a build command here:

    C:\Users\wattbyro\AppData\Roaming\Sublime Text 3\Packages\User\caterrorstxt.sublime-build

That cat h:\errors.txt

```json
{
                "shell_cmd": "type h:\\errors.txt",
                "file_regex": "^([^ ]*?):([0-9][0-9]*):"
}
```

So ctrl-b saves all files and cats h:\errors.txt

Workflow is:

- Edit files,
- Save files
- in nx window:
 - Type make -j10 | sublime.sh in nx
 - wait patiently.
- When make done, back in sublime:
- Hit ctrl-b
- Then F4 to goto-next-error. 

If you've got some errors on the screen and want to use goto next error on them you can go:
                cat | sublime.sh
                then paste the errors into the console,
                then ctrl-d

the script assumes h: is your home directory and z: is the project directory:

My z: is /proj/digi_bby_sw

My h: is /home/wattbyro

note: find in files works (and shows a few lines of contex), but a quicker way is to use grep -n on unix and pretend that the results are compiler errors.  (this is especially useful when working from home when network access is slow)

    find device -name "*.c" | xargs grep -n LCLK_OUT_EN | sublime.sh



bad things about sublime:
=========================
- going to a common method of a class shows that method for all classes (e.g. ctrl-shift-r 'init' is useless)
- no goto-definition for enumerations and variables.

useful packages:
================

## align tabs

add these user settings to add aligning trailing comments: ///<

```json
{
  // To make it easier to remember complex patterns, you can save them in a
  //   dictionary in the settings file. To edit the patterns, launch
  //   Preferences: AlignTab Settings. Use the name as key and the regex as
  //   value. define your own patterns
  "named_patterns": {
     "eq" : "=/f",
     "//" : "\//f",
     "///<" : "\///<f",
  //   right hand side could also be an array of inputs
     "ifthen" : ["=/f", "\\?/f", ":/f"]
  }
}

context menu:
[
   {"caption" : "-"},
    {
      "id": "aligntab",
      "caption": "AlignTab",
      "children": [
          {
          "caption" : "commas",
          "command" : "align_tab",
          "args"    : {"user_input" : ",/f"}
          },
          {
          "caption" : "comment //",
          "command" : "align_tab",
          "args"    : {"user_input" : "\/\//f"}
          },
          {
          "caption" : "comment ///<",
          "command" : "align_tab",
          "args"    : {"user_input" : "\/\/\/</f"}
          },
          {
              "caption": "Exit Table Mode",
              "command": "align_tab_clear_mode"
          }
      ]
  }
]
```

## diagram

hit alt-m to render the selected graphvis/plantuml diagram,... but it opens another window which is a bit awkward,... hitting alt-m renders all plantuml diagrams in the file, but that opens a lot of windows !

VS-CODE has a 'preview' window for plantuml diagrams which is great, but sublime's diagram package is not as nice.

when creating plantuml diagrams, i edit a single file with only the diagram in the file,... then use chrome plantuml plugin to view the file.


license
=======
PMC sierra 3 user license was here: http://intranet.pmc-sierra.bc.ca/wiki/index.php/Sublime_Text_Software 


changing #ifdef MSCC_INTERNAL to display text in grey
=====================================================

C:\Users\wattbyro\AppData\Roaming\Sublime Text 3\Packages\User\C++.sublime-syntax

  preprocessor-rule-disabled-data-structures:
  preprocessor-rule-enabled-data-structures:
  preprocessor-rule-disabled-global:
  preprocessor-rule-enabled-global:
  preprocessor-rule-disabled-statements:
  preprocessor-rule-enabled-statements:

change the match pattern:

    - match: ^\s*((#if)\s+(0))\b

to:

    - match: ^\s*(((#if)\s+(0))|(#ifdef MSCC_INTERNAL)|(#if\s+defined.*MSCC_INTERNAL))\b
