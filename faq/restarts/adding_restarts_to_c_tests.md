\page adding_restarts_to_examples
\brief running examples and tests with restarts

running examples and tests with restarts
========================================

if you call ./scripts/run_examples.sh -restarts -l

this adds the flag restart_and_yaml_compare=log_examples/<example>/restart to the program,

and this does a firmware restart whenever the example switches from doing prov or connects's to doing deprovs or disconnects.

e.g. if the example is:

1. device_init
2. prov
3. prov_lo
4. connect
5. disconnect
6. deprov
7. prov_lo
8. deprov

Then a restart is done before 5. disconnect and before 8.deprov  (g5_api::send monitors the messages being sent and injects 

```c
    g5_api_yaml_print("example_#_before.yaml");
    g5_api_device_detach();
    g5_api_device_attach();
    g5_api_restart();
    g5_api_yaml_print("example_#_after.yaml");
    system( "diff example_#_before.yaml example_#_after.yaml > example_#.diff" );
```

## example

e.g.

    ./scripts/run_examples.sh -l BUILD=revb_pm6000 -restarts g5_bmp_test

creates:

    log_examples/g5_bmp_test/restart_1_before.yaml
    log_examples/g5_bmp_test/restart_1_after.yaml
    log_examples/g5_bmp_test/restart_1.diff

the -restarts flag adds the option restart_and_yaml_compare=logdir/prefix when launching the host example

e.g. in 3 window mode it would be:

    builds/revb_pm6000/bin/g5_bmp_test restart_and_yaml_compare=log_examples/g5_1xotu4_odu4_ilkn_gcc_trans/restart


references
----------
[G5SW-2709 - defects are not bound correctly after a restart](https://jira-agile.microsemi.net:8443/browse/G5SW-2709)

Byron's cheat sheet for running examples with restarts:
-------------------------------------------------------

## run examples

    make parallel_examples BUILD=revb_pm6000 EXAMPLE_ARGS=-restarts

what this does is run all the examples with:

    ./scripts/run_examples.sh BUILD=revb_pm6000 -restarts -l example

## find files with MSCC_ASSERT:

    find log_examples -name '*device*' -type f | xargs grep -l MSCC_ASSERT > ! files

## find the sorts of error messages:

    cat files | xargs cat | grep MSCC_ASSERT | sort -u > ! patterns

## generate a report listing the files that contain each error message

```bash
cat <<'__EOF__' >report.sh
#!/bin/sh
echo
echo ::: $1
X=`echo $1 | sed 's/: .*//'`
cat files | xargs grep -l "$X.*MSCC_ASSERT"
echo
__EOF__
```
    chmod a+x report.sh
    cat patterns | xargs -IF ./report.sh F

## silly command to classify the types of differences:

    find log_examples -name '*.diff' -size +1c \
        | xargs cat \
        | /home/wattbyro/ansifilter-2.10/src/ansifilter \
        | grep '^[+-]' \
        | sed 's/: .*//' \
        | sed 's/  */ /' \
        | sed 's/[[].*//' \
        | sort \
        | uniq -c \
        | sort -n

## counting number of examples that have differences in the yaml files:

    find log_examples -name '*.diff' -size +1c \
        | sed -r 's%log_examples/([^/]*).*%\1%' \
        | sort -u \
        | wc


## looking for an example which has a particular sort of diff:

    find log_examples -name '*.diff' | xargs grep ipt_valid

{code:c}
device/blk/sar_cfg/src/sar_cfg.c:1557: MSCC_ASSERT( FALSE, 0 );
{code}
which happens when you run these examples:
{code:bash}
 ./scripts/run_examples.sh BUILD=revb_pm6000 -restarts -l g5_flexe_cfg
 ./scripts/run_examples.sh BUILD=revb_pm6000 -restarts -l g5_line_ipts_nfic_190_channels_test
 ./scripts/run_examples.sh BUILD=revb_pm6000 -restarts -l g5_flexe_phy_aware_gtest
 ./scripts/run_examples.sh BUILD=revb_pm6000 -restarts -l g5_otucn_100ge_xponder
{code}

{code:c}
device/blk/sar_cfg/src/sar_cfg.c:1940: MSCC_ASSERT( FALSE, 0 );
{code}
which happens when you run g5_nfic_test:
{code:bash}
 ./scripts/run_examples.sh BUILD=revb_pm6000 -restarts -l g5_nfic_test
{code}


{code:c}
device/blk/sts_sw/src/sts_sw.c:1459: MSCC_ASSERT( (num_ts <= MAX_TIMESLOTS_PER_ODU) && (num_ts > 0), STS_SW_ERR_INVALID_{code}
which happens when you run g5_coreotn_jk_reprov_test:
{code:bash}
PARAMETERS );
 ./scripts/run_examples.sh BUILD=revb_pm6000 -restarts -l g5_coreotn_jk_reprov_test
{code}

{code:c}
device/feature_mgr/src/g5_dev_module_config.c:1954: MSCC_ASSERT( FALSE, 0 );
{code}
which happens when you run g5_fpdl_1x100ge_odu4_ilkn:
{code:bash}
 ./scripts/run_examples.sh BUILD=revb_pm6000 -restarts -l g5_fpdl_1x100ge_odu4_ilkn
{code}

{code:c}
device/feature_mgr/src/g5_dev_segment_otn_base.c:1020: MSCC_ASSERT( FALSE, 0 );
{code}
which happens when you run g5_serdes_test or g5_bringup_serdes:
{code:bash}
 ./scripts/run_examples.sh BUILD=revb_pm6000 -restarts -l g5_serdes_test
 ./scripts/run_examples.sh BUILD=revb_pm6000 -restarts -l g5_bringup_serdes
{code}
