workflow for debugging python restart failures {#faster_restart_debugging}
==============================================

Ahmed or Dave says use this python command:

    python3.6 -u -m pytest ../../g5tests/sw/testcases/otn/test_otn_gfec.py -x --testrunner_mode RESTART_ON_ALL_API_CALL --testrunner_args='-op_id 26 -fail_first_mismatch' --verbosity=WARN --regression_mode --skip_svpf_init -k 'test_gfec_OTUCn_line[0-gfec_1port_OTUC1_line]'

run that and observe the failure.

note this creates a flight recorder file in g5/startup

you can re-run with flight recorder:

    ./flightrecorder versioncheck=0 device_id=0 enable_workaround playback=/proj/digi_bby_sw2/users/wattbyro/g5/g5_env/g5/startup/TestOTUCnGFec__test_gfec_OTUCn_line__0-gfec_1port_OTUC1_line.binlog

which only takes 25 seconds to run.

running the flight recorder generates a numbered version of 'all_vars_print.xxx'

    all_vars_print.106
    all_vars_print.102

use diff to check the differences:

```diff
    gdiff all_vars_print.102 all_vars_print.106
    + git --no-pager diff --patience --no-index --unified=10 --color=always -w all_vars_print.102 all_vars_print.106
    diff --git a/all_vars_print.102 b/all_vars_print.106
    index 270faab..d7316f5 100644
    --- a/all_vars_print.102
    +++ b/all_vars_print.106
    @@ -1583027,35 +1583027,35 @@ g5_dev_segment_ilkn_pkt:
         pmon: 0x01b00108
         pmon: 0x01b0010c
         pmon: 0x01b00110
         pmon: 0x01b01030
         pmon: 0x0155d800
         chnl=0xfa000000, segment: 6
     g5_dev_segment_otucn_group: 
         _unavail_serdes_pin_mask: 0 (0x0)
         _member_chnl_mask: 1 (0x1)
         _mode: G5_OTUCN_MODE_CN_MUXED (0)
    -    _gfec_mode: G5_OTN_FEC_MODE_CORRECTION (1)
    +    _gfec_mode: G5_OTN_FEC_MODE_PASSTHRU (2)
         _prov_mode: G5_OTN_PROV_MODE_LINEOTN (0)
         _otucn_intf: G5_OTUCN_OTUC_INTF (0)
         _group_master_dci_stream: 0 (0x0)
         _num_ports: 1 (0x1)
         _group_cfg_data: 
             group_data: 
                 group_chnl_id: 4194304000 (0xfa000000)
                 info: 
                     mode: G5_OTUCN_MODE_CN_MUXED (0)
                     prov_mode: G5_OTN_PROV_MODE_LINEOTN (0)
                     otucn_intf: G5_OTUCN_OTUC_INTF (0)
                     rx_flexo_group_id: 4294967295 (0xffffffff)
                     tx_flexo_group_id: 4294967295 (0xffffffff)
    -                gfec_mode: G5_OTN_FEC_MODE_CORRECTION (1)
    +                gfec_mode: G5_OTN_FEC_MODE_PASSTHRU (2)
             port_data[0]: 
                 chnl_id: 0 (0x0)
                 group_chnl_id: 4194304000 (0xfa000000)
                 serdes_port_id: 0 (0x0)
                 info: 
                     rx_phy_number: 4294967295 (0xffffffff)
                     tx_phy_number: 4294967295 (0xffffffff)
                     tx_unavailable_slot_mask: 0 (0x0)
                     pin_mode: G5_SERDES_PIN_MODE_NRZ (0)
             port_data[1]: 
```

run scripts/run_binlog_replay.sh to create a 'replay.c' file from the binlog
============================================================================

To debug in simulation you can use a simulation version of the flight recorder, or use the 

    ./scripts/run_binlog_replay.sh /proj/digi_bby_sw2/users/wattbyro/g5/g5_env/g5/startup/TestOTUCnGFec__test_gfec_OTUCn_line__0-gfec_1port_OTUC1_line.binlog

running with flight recorder each time is fine too,... but looking at the equivalent replay.c file can be interesting.


then use a 2 window setup if testing with 2lab or a 3 window setup if testing with simulation:

window1:

    builds/pm6000_revb/bin/g5_simulator

window2:

    gdb -ex 'b g5_dev_segment_otucn_group::restart' --args builds/revb_pm6000/bin/device

window3:

    setenv MSCC_FW_CMD "echo restart device now"
    builds/revb_pm6000/bin/TestOTUCnGFec__test_gfec_OTUCn_line__0-gfec_1port_OTUC1_line notimeout


When the test hits the fw_restart command, go back to window 2 and hit 'r' again.
