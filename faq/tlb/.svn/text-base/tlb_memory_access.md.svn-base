TLB format and A9 Memory configuration
==================================

The memory blocks and memory access of the A9 chip are configured inside the start_test.asm file.
The memory configuration and access permissions are defined using the Short-descriptor translation table format ( see *ARM Architecture Reference Manual* p.1326 )

![Short Descriptor](images/short_desc.png)

There are several memory configurations

- WT - Write-Through
- WB - Write-Back
- WA - Write-Allocate
- SO - Strongly-ordered
- DV - Device
- -S - Shared

        LDR     r1,  =0x0001100E ;  Shared+TEX+CB+Section for WB-WA-S
        LDR     r2,  =0x00010002 ;  Shared+TEX+CB+Section for SO-S
        LDR     r3,  =0x00010006 ;  Shared+TEX+CB+Section for DV-S
        LDR     r4,  =0x00010DFA ;  Shared+TEX+CB+Section for WT-S-XN-D15-AP_RW
        LDR     r5,  =0x0001000A ;  Shared+TEX+CB+Section for WT-S
        LDR     r6,  =0x0001000E ;  Shared+TEX+CB+Section for WB-S
        LDR     r7,  =0x0000100E ;  Shared+TEX+CB+Section for WB-WA
        LDR     r8,  =0x00011DFE ;  Shared+TEX+CB+Section for WB-WA-S-XN-D15-AP_RW
        LDR     r10, =0x00010DF6 ;  Shared+TEX+CB+Section for DV-S-XN-D15-AP_RW
        LDR     r11, =0x0000000E ;  Shared+TEX+CB+Section for WB

The memory region attributes are specified using the TEX[2:0], C and B bits. More information about which bits produce which configuration can be found in the ARM manual.
( see *ARM Architecture Reference Manual* p.1365 )

To use these bits to set the memory attributes, the TRE bit in the SCTLR register must be set to '0'. ( see *ARM Architecture Reference Manual* p.1366 )

Bits such as XN (Execute-Never) and AP (Access Permission) can be used to set the permission setting of specific pages/blocks of memory.

*XN* (Execute-Never) - When the XN bit is 1, a Permission fault is generated if the processor attempts to execute an instruction fetched from the corresponding memory region.

![AP bits](images/ap_permissions.png)

To allow these bits to set memory access premissions, the Domain used in the translation table needs to be configured as CLIENT.

There are 16 domains, and each can be configured using the bits in the DACR register, as either No Access, Client or Manager. ( see *ARM Architecture Reference Manual* p.1558 )

- **0b00** No access. Any access to the domain generates a Domain fault.
- **0b01** Client. Accesses are checked against the permission bits in the translation tables.
- **0b10** Reserved, effect is UNPREDICTABLE.
- **0b11** Manager. Accesses are not checked against the permission bits in the translation tables.

Writing to the DACR register
```
MRC p15, 0, <Rt>, c3, c0, 0 ; Read DACR into Rt
MCR p15, 0, <Rt>, c3, c0, 0 ; Write Rt to DACR
```
*start_test.asm*
```
;; Don't check rights for all but D15
MOV     r0, #0x7FFFFFFF ; set Domain 15 to CLIENT
MCR     p15, 0, R0, c3, c0, 0
```
The **PXN** (Privileged execute-never) bit can be used to stop execution from a specific region in Privileged Mode, IF this functionality is supported.

Setting this bit in our configuration causes the device to crash. Given this result, it is assumed that we do not support this functionality. The manual says to set this bit to '0' if the functionality is not supported. This might be the reason why setting this bit to '0' works but setting it to '1' does not. The CPU might crash if it sees an invalid parameter being set.

The PXN bit was set for several memory regions to test and see if it only crashed for a specific region but the result was the same. **The device did not start after the firmware was loaded.**

## Additional Features

An additional feature that can be configured is *L1/L2 cache prefetching*. This can be done by setting the appropriate bits inside the **ACTLR** register.

![ACTLR](images/actlr.png)

In the current implementation pre-fetching is disabled.

The A9 also has a *program flow prediction* feature. When this feature is enabled, the CPU will try to predict the next instruction. This feature can be enabled by setting the Z bit inside the **SCTLR** register.

![SCTLR](images/sctlr.png)

In our implementation, flow-prediction is enabled.