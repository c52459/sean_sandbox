hunting for which version breaks something
==========================================

you could svn co -r REVISION  http://bby-svn:3693/svn/PM71_85_252_01/trunk/code rREVISION

or just svn up -r REVISION to each revision.

1. find a range of revisions since something worked to when it broke:

```bash
svn log -q -r 88523:HEAD | grep ^r | sed -r 's/^r(\w*).*/\1/' >revisions
```

2. Create a script to do something with that revision

```bash
cat << '__EOF__' > test_revision.sh
#!/bin/bash
svn up -r $1
make clean
make flab
__EOF__
```

    chmod a+x test_revision.sh

3. run the script with every revision

    cat revisions | xargs -IREV sh -x test_revision.sh REV

4. look for when it started to fail:

    find log_unit_tests* -name unit_test.log | xargs grep FAILED