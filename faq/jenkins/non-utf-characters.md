looking for non utf-8 characters
================================

looking for non-utf-8 characters:

    grep -P "[\x80-\xFF]"  

looking for non utf-8 characters and tabs:

    grep --color=always -P "[\x80-\xFF\x09]" faq/defect_monitor/defect_monitor.md

replacing tabs with spaces:

    perl -p -i -e 's/\t/    /g' faq/defect_monitor/defect_monitor.md

i tried to create this script (hacked from util.pm) to convert common unicode characters, but it doesn't seem to be working:

    perl -p -i \
        -e "s/\x{2018}/'/gs;" \
        -e "s/\x{2019}/'/gs;" \
        -e "s/\x{91}/'/gs;" \
        -e "s/\x{92}/'/gs;" \
        -e 's/\x{2126}/Ohm/gs;' \
        -e 's/\x{2026}/.../gs;' \
        -e 's/\x{b7}/./gs;' \
        -e 's/\x{2013}/-/gs;' \
        -e 's/\x{2014}/--/gs;' \
        -e 's/\x{2015}/--/gs;' \
        -e 's/\x{fffd}/<?>/gs;' \
        -e 's/\x{b5}/u/gs;' \
        -e 's/\x{96}/-/gs;' \
        -e 's/\x{3bc}/u/gs;' \
        -e 's/\x{201d}/"/gs;' \
        -e 's/\x{2022}/*/gs;' \
        -e 's/\x{f0f0}/<?>/gs;' \
        -e 's/\x{201c}/"/gs;' \
        -e 's/\x{2265}/>=/gs;' \
        -e 's/\x{2264}/<=/gs;' \
        -e 's/\x{3B1}/{alpha}/gs;' \
        -e 's/\x{3B2}/{beta}/gs;' \
        -e 's/\x{A0}/ /gs;' \
        -e 's/\x{fb01}/fi/gs;' \
        -e 's/\x{2190}/<--/gs;' \
        -e 's/\x{b0}/(deg)/gs;' \
        -e 's%\x{b1}%+/-%gs;' \
        -e 's/\x{d7}/x/gs;' \
        -e 's/\x{201f}/  /gs;' \
        -e 's%\x{f7}%"%gs;' \
        -e 's/\x{3a9}/{Omega}/gs;'
