debugging a stuck examples_pm6017 job
=====================================

## examples look stuck:

* http://bby1swbuild03:8080/job/g5_sw_build/job/g5_sw_trunk_run_examples_pm6017/lastBuild/console

hovering over the progress bar of run_examples_pm6017
we see it has been running for 10 hours,
so we want to find out why it is stuck.


## console shows:

Building remotely on bby1swbuild05_slave

And we see a bunch of examples have finished,
but it doesn't show what examples are still running.


## so we ssh to bby1swbuild05

## switch users to svccpdbd

    su svccpdb

## take a look at what files are hung:

    ps auxf

## find the offending process

    svccpdbd  8825  2.9  0.3 19240080 1910444 ?    Sl    2019 1985:20  |           \_ java -d64 -Xms1024m -Xmx6144m -Dhudson.remoting.Launcher.pingIntervalSec=-1 -jar remoting.jar -workDir /nobackup/jenkins3_slave -jar-cache /nobackup/jenkins3_slave/remotin
    svccpdbd 59886  0.0  0.0   9244  1132 ?        S    Jan29   0:00  |               \_ /bin/sh -xe /tmp/jenkins7024945239443367259.sh
    svccpdbd 59889  0.0  0.0  15816 12488 ?        S    Jan29   0:00  |               |   \_ /usr/bin/make jenkins_run_examples_pm6017
    svccpdbd 59939  0.0  0.0  15964 12652 ?        S    Jan29   0:00  |               |       \_ /usr/bin/make parallel_examples BUILD=sim_pm6017_revb
    svccpdbd 59975  0.0  0.0   9404  1332 ?        S    Jan29   0:00  |               |           \_ /bin/sh -c /usr/bin/make -k -j10 BUILD=sim_pm6017_revb DEVICE_BUILD= EXAMPLE_ARGS="" log_examples/g5_ilkn_test/test_result log_examples/g5_dcpb_loopback_pv_
    svccpdbd 59976  0.3  0.0  82544 79252 ?        S    Jan29   2:27  |               |               \_ /usr/bin/make -k -j10 BUILD=sim_pm6017_revb DEVICE_BUILD= EXAMPLE_ARGS= log_examples/g5_ilkn_test/test_result log_examples/g5_dcpb_loopback_pv_ipts_pkt/
    svccpdbd 39972  0.0  0.0   9412  1216 ?        S    00:02   0:00  |               |                   \_ /bin/sh -c scripts/run_examples.sh -nocompile -l  BUILD=sim_pm6017_revb DEVICE_BUILD= g5_flexe_otn_cbr_unaware_gtest &> log_examples/g5_flexe_otn_cb
    svccpdbd 39975  0.0  0.0   9556  1640 ?        S    00:02   0:00  |               |                   |   \_ /bin/bash scripts/run_examples.sh -nocompile -l BUILD=sim_pm6017_revb DEVICE_BUILD= g5_flexe_otn_cbr_unaware_gtest
    svccpdbd 40245 50.9  0.0  68704 40972 ?        R    00:02 321:38  |               |                   |       \_ builds/sim_pm6017_revb/bin/g5_flexe_otn_cbr_unaware_gtest
    svccpdbd 48360  0.0  0.0   9412  1220 ?        S    00:04   0:00  |               |                   \_ /bin/sh -c scripts/run_examples.sh -nocompile -l  BUILD=sim_pm6017_revb DEVICE_BUILD= g5_1xotu2_10g_line_enet_ilkn_pkt_cfg &> log_examples/g5_1xotu2
    svccpdbd 48361  0.0  0.0   9556  1636 ?        S    00:04   0:00  |               |                       \_ /bin/bash scripts/run_examples.sh -nocompile -l BUILD=sim_pm6017_revb DEVICE_BUILD= g5_1xotu2_10g_line_enet_ilkn_pkt_cfg
    svccpdbd 48694 56.8  0.0  68100 40456 ?        S    00:04 358:16  |               |                           \_ builds/sim_pm6017_revb/bin/g5_1xotu2_10g_line_enet_ilkn_pkt_cfg

## find the directory where it is logging using *lsof*

lsof -p 40245

    svccpdbd@bby1swbuild05.microsemi.net% lsof -p 40245
    COMMAND     PID     USER   FD   TYPE     DEVICE    SIZE/OFF       NODE NAME
    g5_flexe_ 40245 svccpdbd  cwd    DIR      253,4        4096   88092658 /usr/export/home/bby1swbuild05/nobackup/jenkins3_slave/workspace/g5_sw_build/g5_sw_trunk_run_examples_pm6017
    g5_flexe_ 40245 svccpdbd  rtd    DIR      253,0        4096          2 /
    g5_flexe_ 40245 svccpdbd  txt    REG      253,4    10914859   88085922 /usr/export/home/bby1swbuild05/nobackup/jenkins3_slave/workspace/g5_sw_build/g5_sw_trunk_run_examples_pm6017/builds/sim_pm6017_revb/bin/g5_flexe_otn_cbr_unaware_gtest
    g5_flexe_ 40245 svccpdbd  mem    REG      253,0      161776     262175 /lib64/ld-2.12.so
    g5_flexe_ 40245 svccpdbd  mem    REG      253,0     1930416     262257 /lib64/libc-2.12.so
    g5_flexe_ 40245 svccpdbd  mem    REG      253,0      600048     262687 /lib64/libm-2.12.so
    g5_flexe_ 40245 svccpdbd  mem    REG      253,0       23088     262582 /lib64/libdl-2.12.so
    g5_flexe_ 40245 svccpdbd  mem    REG      253,0      146592     262258 /lib64/libpthread-2.12.so
    g5_flexe_ 40245 svccpdbd  mem    REG      253,0       47760     262702 /lib64/librt-2.12.so
    g5_flexe_ 40245 svccpdbd  mem    REG      253,0       93352     298734 /lib64/libgcc_s-4.4.7-20120601.so.1
    g5_flexe_ 40245 svccpdbd  mem    REG      253,0      989840     408481 /usr/lib64/libstdc++.so.6.0.13
    g5_flexe_ 40245 svccpdbd  DEL    REG       0,16             1303754851 /dev/shm/digi_g5_device_35106
    g5_flexe_ 40245 svccpdbd    0r  FIFO        0,8         0t0 1303263364 pipe
    g5_flexe_ 40245 svccpdbd    1w   REG      253,4 13047803281   88086341 /usr/export/home/bby1swbuild05/nobackup/jenkins3_slave/workspace/g5_sw_build/g5_sw_trunk_run_examples_pm6017/log_examples/g5_flexe_otn_cbr_unaware_gtest/example.log
    g5_flexe_ 40245 svccpdbd    2w   REG      253,4 13047803281   88086341 /usr/export/home/bby1swbuild05/nobackup/jenkins3_slave/workspace/g5_sw_build/g5_sw_trunk_run_examples_pm6017/log_examples/g5_flexe_otn_cbr_unaware_gtest/example.log
    g5_flexe_ 40245 svccpdbd    3u  IPv4 1303758875         0t0        TCP localhost:45484->localhost:35106 (ESTABLISHED)
    g5_flexe_ 40245 svccpdbd    4u  IPv4 1303758881         0t0        TCP localhost:45488->localhost:35106 (ESTABLISHED)
    g5_flexe_ 40245 svccpdbd    5u   REG       0,16     8388608 1303754851 /dev/shm/digi_g5_device_35106 (deleted)
    g5_flexe_ 40245 svccpdbd    6u   REG       0,16     8388608 1303754851 /dev/shm/digi_g5_device_35106 (deleted)

## gdb the example or simulator if you want:

gdb --pid 40245

    (bt)
    #6  0x000000000045cb2d in g5_api_pmon_is_pmon_ready (device_id=25891) at host/g5_api/src/g5_api_pmon.c:190
    #7  0x000000000044bfa9 in dump_its_pmon_counts (device_id=25891, ilkn_chnl_id=4096, print_top=false, packet_counts_ptr=0x0) at examples/g5_1xotu2_10g_line_enet_ilkn_pkt_cfg/src/g5_1xotu2_10g_line_enet_ilkn_pkt_cfg.c:167
    #8  0x000000000044a25e in g5_ipts_test_setup_and_run (device_id=25891, ilkn_chnl_id=4096, otu_chnl_id=12288, gen_direction=G5_IPTS_LOCATION_FABRIC_EGRESS, mon_direction=G5_IPTS_LOCATION_FABRIC_INGRESS, packet_counts_ptr=0x7ffd05286d30)
        at examples/g5_1xotu2_10g_line_enet_ilkn_pkt_cfg/src/g5_1xotu2_10g_line_enet_ilkn_pkt_cfg.c:270
    #9  0x0000000000449c67 in g5_otu2_10ge_ilkn_pkt_path_cfg (device_id=25891, fc_enable=true) at examples/g5_1xotu2_10g_line_enet_ilkn_pkt_cfg/src/g5_1xotu2_10g_line_enet_ilkn_pkt_cfg.c:695
    #10 0x0000000000449516 in main (argc=1, argv=0x7ffd05287288) at examples/g5_1xotu2_10g_line_enet_ilkn_pkt_cfg/src/g5_1xotu2_10g_line_enet_ilkn_pkt_cfg.c:827

    (gdb) list
    166 
    167     while (!g5_api_pmon_is_pmon_ready(device_id))
    168     {
    169         MSCC_OS_USLEEP(1000);
    170     }


## look at the log files:
    svccpdbd@bby1swbuild05.microsemi.net% ls -lh /usr/export/home/bby1swbuild05/nobackup/jenkins3_slave/workspace/g5_sw_build/g5_sw_trunk_run_examples_pm6017/log_examples/g5_flexe_otn_cbr_unaware_gtest
    total 130G
    -rw-r--r-- 1 svccpdbd eng  21K Jan 30 00:02 console.log
    -rw-r--r-- 1 svccpdbd eng  14K Jan 30 00:02 device.log
    -rw-r--r-- 1 svccpdbd eng  13G Jan 30 10:39 example.log
    -rw-r--r-- 1 svccpdbd eng 118G Jan 30 10:39 simulator.log
    -rw-r--r-- 1 svccpdbd eng  234 Jan 30 00:02 test_result_failed

## device.log

    19. host_msg: PMON_MODULE PMON_MANUAL_TRIGGER 0xffffffff
    device/blk/digihal/src/digihal.c:350: MSCC_RUNTIME_ERROR( DIGIHAL_ERR_INVALID_ADDRESS )
    backtrace: digihal_pmon_batch_read <- gdma::copy_section_2_spram <- pmon::collect_otn_values <- pmon::collect_values <- pmon::collect_all_values <- pmon::pmon_lclk_intr_callback <- pmon_task::background_task_entry_point <- mt_async_task::async_task_loop <- mt_async_task::all_async_tasks_start_here
    
    ASSERT: ERROR in function digihal_pmon_batch_read, file device/blk/digihal/src/digihal.c, line 351
    
    device/blk/digihal/src/digihal.c:351: MSCC_ASSERT( 0, DIGIHAL_ERR_INVALID_ADDRESS );
    
    backtrace: 0 digihal_pmon_batch_read
    backtrace: 1 gdma::copy_section_2_spram
    backtrace: 2 pmon::collect_otn_values
    backtrace: 3 pmon::collect_values
    backtrace: 4 pmon::collect_all_values
    backtrace: 5 pmon::pmon_lclk_intr_callback
    backtrace: 6 pmon_task::background_task_entry_point
    backtrace: 7 mt_async_task::async_task_loop
    backtrace: 8 mt_async_task::all_async_tasks_start_here
    
    device/blk/digihal/src/digihal.c:351: ASSERT: 0x4e09
    device: host/porting/sim/src/mscc_sw_simulation.c:1968: volatile void mscc_sw_simulation_assert(MSCC_ERROR, const char *, const char *, const char *, const char *, UINT32, const char *, const char *, ...): Assertion `false' failed.

## example.log

    Function=g5_api_pmon_is_pmon_ready(0x6523 )
    Function=g5_api_pmon_is_pmon_ready(0x6523 )
    Function=g5_api_pmon_is_pmon_ready(0x6523 )
    Function=g5_api_pmon_is_pmon_ready(0x6523 )
    Function=g5_api_pmon_is_pmon_ready(0x6523 )
    Function=g5_api_pmon_is_pmon_ready(0x6523 )
    Function=g5_api_pmon_is_pmon_ready(0x6523 )
    Function=g5_api_pmon_is_pmon_ready(0x6523 )
    Function=g5_api_pmon_is_pmon_ready(0x6523 )
    Function=g5_api_pmon_is_pmon_ready(0x6523 )

## simulation.log

    -- stats as of 5428288 seconds --
    5428288.009477900 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" PACKET STATS: READ:91 ADVANCE_SIMULATION_TIME:182 
    5428288.009477900 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" TIME STATS: time_advances:1.001 seconds 
    5428288.009478000 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 1.00 milliseconds
    5428288.010478000 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 10.00 milliseconds
    5428288.020478100 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 1.00 milliseconds
    5428288.021478100 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 10.00 milliseconds
    5428288.031478200 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 1.00 milliseconds
    5428288.032478200 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 10.00 milliseconds
    5428288.042478300 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 1.00 milliseconds
    5428288.043478300 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 10.00 milliseconds
    5428288.053478400 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 1.00 milliseconds
    5428288.054478400 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 10.00 milliseconds
    5428288.064478500 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 1.00 milliseconds
    5428288.065478500 H#16 "g5_1xotu2_10g_line_enet_ilkn_pkt_cfg" advancing simulation time by 10.00 milliseconds



## conclusion:

in this particular case the tail of the device.log shows that it crashed in pmon trigger,
and the host was in a while loop waiting for pmon done, and the simulator was moving time forward as fast as it could.

## possible steps to mitigate this in the future:

1. change g5_simulator to stop after a certain amount of simulated time.
