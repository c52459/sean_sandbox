some useful lsf commands
========================

I have a script called bjobsl which is this:

    bjobs -o 'jobid: user: stat: exec_host: queue:40 submit_time: start_time: sub_cwd:200 job_name:120' $* \
      | sed -e 's%\S*/workspace/[^/]*/%%' -e 's%[a-zA-Z0-9/_.]*g4_sw_fw%g4_sw_fw%' -e 's/ *$//' -e 's/   */  /g'

This displays the command that was issued and the start time which are the fields I'm usually interested in.

The sed script changes:

     /work/bby1swbuild02/nobackup/jenkins3_slave/workspace/g5_sys_test/pm6000_device_revb_lsf_regression_sanity_restart/g5tests/sw/scripts 
to:

     pm6000_device_revb_lsf_regression_sanity_restart/g5tests/sw/scripts 

Find all revb1 hosts
====================

    bhosts -R revb1 | awk '{print $1}' | tee patterns

## Find everyone using those revb1 boards

    bjobs -u all -r | grep -f patterns

The normal queue (4 hours)
==========================

    bsub -Ip -q normal -R g5 -app g5 terminal

The short queue (30 minutes)
============================

    bsub -Ip -q short -R g5 -app g5 terminal

running a command on every board
================================

    maybe get a list of hosts, and run bsub -m <host> and output the results to a file based on the host name?
    
