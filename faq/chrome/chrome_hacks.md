chrome hacks
============

keyword space
-------------

when I want to navigate to say G5SW-1492, I usually start typing G5SW, then it chrome guesses some jira number,... then i delete everything back to the G5SW-, then paste in my jira number,... instead you can simply type jira space or tab with this search engine hack:


navigate to:

    chrome://settings/searchEngines

or use 'settings -> search engine -> manage search engines'

beside "Other search engines", click 'add'.

enter the following for search engine, keyword and url:

| search engine | keyword    | URL with %s in place of query                                              |
| ------------- | -------    | -----------------------------                                              |
|    jira       | jira       | https://jira-agile.microsemi.net:8443/browse/%s                            |
|    cc         | cc         | http://ccollab:5700/ui#review:id=%s                                        |
|    peps       | peps       | http://bby1web03.pmc-sierra.internal/intranet/peps/prep_edit.cfm?number=%s |
|    weather    | weather    | https://www.google.com/search?q=weather.gc.ca %s                           |
|    confluence | confluence | https://confluence.microchip.com/dosearchsite.action?queryString=%s        |


there's some weird java script to copy/paste this information, but it sounds a bit complicated.