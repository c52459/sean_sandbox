Example Showing MPMO calendar during G.HAO
======================

There are many calendars that have to be "stepped" during G.HAO.  This requires pre-allocationing the entries in the CDU in LCR, and "stepping" the entries in BWR.

This example is meant to show how the MPMO_DP calendar is handled.  There are many other calendars that follow similar principles.

# Initialization

MPMO CDU and entry builder are initialized with G.HAO enabled configuration.

```plantuml
@startuml
skinparam ParticipantPadding 40
skinparam BoxPadding 40
'--------- participants -------------
box "Firmware" #moccasin
  participant "calendar_manager_device" as calendar_manager_device
  participant "calendar_day_usage" as calendar_day_usage
  participant "entry_builder_interface" as entry_builder_interface
endbox

'--------- call sequence diagram -------------
calendar_manager_device -> calendar_day_usage : handle_init()
note over calendar_day_usage
  CDU ID : SAR_DP_MPMO_SCBS4_CDU_ID
  handle_init() called with:
   cfg.ghao==TRUE
end note
calendar_manager_device -> entry_builder_interface : scbs_basic_builder_create()
note over entry_builder_interface
  scbs_basic_builder_create() called with:
  ghao_1ts_update==TRUE
end note
hide footbox
@enduml
```


# LCR (showing increase scenario)

In LCR, CDU G.HAO context is initialize.  CDU calendar entries are pre-allocated.

```plantuml
@startuml
skinparam ParticipantPadding 40
skinparam BoxPadding 40
'--------- participants -------------
box "host" #cyan
  actor "GHAO" as ghao
endbox
box "Firmware" #moccasin
  participant "g5_dev_ghao_mgr" as g5_dev_ghao_mgr
  participant "g5_dev_segment_ilkn_cbr" as g5_dev_segment_ilkn_cbr
  participant "calendar_manager_ilkn_cbr" as calendar_manager_ilkn_cbr
  participant "calendar_day_usage" as calendar_day_usage
  participant "placement_interface" as placement_interface
endbox

'--------- call sequence diagram -------------
ghao -> g5_dev_ghao_mgr : g5_api_ghao_en_set()
ghao -> g5_dev_ghao_mgr : g5_api_ghao_lcr_bwr_cfg()
g5_dev_ghao_mgr -> g5_dev_ghao_mgr : lcr_size_tx_inc_rp()
g5_dev_ghao_mgr -> g5_dev_ghao_mgr : chnl_calendar_day_init()
g5_dev_ghao_mgr -> g5_dev_segment_ilkn_cbr : ghao_calendar_day_init()
g5_dev_segment_ilkn_cbr -> calendar_manager_ilkn_cbr : ghao_dcpb_sar_calendar_day_init()
calendar_manager_ilkn_cbr -> calendar_day_usage : ghao_chnl_cal_days_init()
calendar_day_usage -> calendar_day_usage : ghao_chnl_cal_days_init()
note over calendar_day_usage
  sync starting calendar ctxt_cdu 
  with ghao_ctxt.

  Copies only G.HAO channel entries from:
     ctxt_cdu->calendar
  Into:
     ghao_ctxt->past_calendar
     ghao_ctxt->step_calendar
end note
ghao -> g5_dev_ghao_mgr : g5_api_ghao_lcr_bwr_cfg()
g5_dev_ghao_mgr -> g5_dev_ghao_mgr : lcr_size_tx_inc()
g5_dev_ghao_mgr -> g5_dev_segment_ilkn_cbr : chnl_calendar_day_update()
g5_dev_segment_ilkn_cbr -> calendar_manager_ilkn_cbr : ghao_calendar_day_update()
calendar_manager_ilkn_cbr -> calendar_day_usage : ghao_dcpb_sar_calendar_day_update()
calendar_day_usage -> calendar_day_usage : chnl_cal_days_add()
calendar_day_usage -> placement_interface : chnl_days_add()
placement_interface -> placement_interface : chnl_days_remove()
placement_interface -> placement_interface : num_days_start_of_list_move()
placement_interface -> calendar_day_usage : ghao_chnl_cal_days_init()
note over calendar_day_usage, placement_interface
  If placement interface has moved existing entries
  sync ctxt_cdu with ghao_ctxt before adding new entries 
  by calling ghao_chnl_cal_days_init()
end note
placement_interface -> placement_interface : num_days_start_of_list_move()
note over placement_interface
  Add new entries in ctxt_cdu
  (will contain end calendar in
  ctxt_cdu)
end note
hide footbox
@enduml
```



# BWR (showing increase scenario)

In BWR, entries are stepped by using CDU G.HAO context.

The CDU G.HAO is initialize once the ramp has completed.

Note, not all BWR steps are shown.

```plantuml
@startuml
skinparam ParticipantPadding 40
skinparam BoxPadding 40
'--------- participants -------------
box "host" #cyan
  actor "GHAO" as ghao
endbox
box "Firmware" #moccasin
  participant "g5_dev_ghao_mgr" as g5_dev_ghao_mgr
  participant "GHAO_THREAD" as ghao_thread
  participant "g5_dev_segment_ilkn_cbr" as g5_dev_segment_ilkn_cbr
  participant "calendar_manager_ilkn_cbr" as calendar_manager_ilkn_cbr
  participant "calendar_day_usage" as calendar_day_usage
  participant "entry_builder_interface" as entry_builder_interface
  participant "SCBS4" as scbs4
endbox

'--------- call sequence diagram -------------
ghao -> g5_dev_ghao_mgr : g5_api_ghao_lcr_bwr_cfg()
g5_dev_ghao_mgr -> g5_dev_ghao_mgr : step_bwr_setup()
g5_dev_ghao_mgr -> g5_dev_ghao_mgr : fw_segment_init()
ghao_thread -> ghao_thread : fw_tick_map_inc()
ghao_thread -> g5_dev_ghao_mgr : chnl_calendar_day_step()
g5_dev_ghao_mgr -> g5_dev_segment_ilkn_cbr : ghao_calendar_day_step()
g5_dev_segment_ilkn_cbr -> calendar_manager_ilkn_cbr : ghao_dcpb_sar_calendar_day_step()
calendar_manager_ilkn_cbr -> calendar_day_usage : ghao_chnl_cal_days_step()
note over calendar_day_usage
  Take first availble entry from ctxt_cdu->calendar
  and add entries to ghao_ctxt->step_calendar
end note
ghao_thread -> scbs4 : mpmo_scbs3_page_update_swap()
scbs4 -> scbs4 : offline_standby_cal_mem_update()
scbs4 -> entry_builder_interface : make_row()
entry_builder_interface->calendar_day_usage : calendar_ghao_step_chnl_by_day_get()
note over entry_builder_interface
  calendar_ghao_step_chnl_by_day_get() 
  called if ghao_1ts_update set.  
  Gets ghao_ctxt->step_calendar

  Note, If entries set to MAX in ghao_ctxt->step_calendar(),
  calendar_chnl_by_day_get() is used to support normal
  prov case.
end note
scbs4 -> scbs4 : scbs_field_STBY_CALENDAR_DAT_set()
note over ghao_thread, scbs4
  Repeat steps until all entries added
end note
ghao -> g5_dev_ghao_mgr : g5_api_ghao_lcr_bwr_cfg()
g5_dev_ghao_mgr -> g5_dev_ghao_mgr : bwr_ramp_end_tx_3_inc_rp()
g5_dev_ghao_mgr -> g5_dev_ghao_mgr : chnl_calendar_day_clean()
g5_dev_ghao_mgr -> g5_dev_segment_ilkn_cbr : ghao_calendar_day_clean()
g5_dev_segment_ilkn_cbr -> calendar_manager_ilkn_cbr : ghao_dcpb_sar_calendar_day_clean()
calendar_manager_ilkn_cbr -> calendar_day_usage : ghao_chnl_cal_days_clean()
note over calendar_day_usage
  Set ghao_ctxt->step_calendar and ghao_ctxt->past_calendar to MAX

  Note, This is also called if G.HAO is aborted.
end note
hide footbox
@enduml
```
