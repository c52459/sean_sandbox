watchdog workflow
=================

Diagrams in this file are best viewed with:

    ./scripts/run_doxygen.sh internal faq/watchdog/workflow.md

And point browser to:

    file://bbynetappc1nas02.microsemi.net/digi_bby_sw2/users/your_workspace/doxygen_output/html/md_faq_watchdog_workflow.html

overview
========

there are two watchdogs but currently we are only using 1

The primary reason to have a watchdog is so that if the arm dies, the pcie link negotiation will work if 
the host is rebooted.

A firmware assert should still permit the host to capture log files, so we want the old OCR to stick around 
until the console, stats and dbg_log are captured.

There are four ways the watchdog can happen:

1. an MSCC_ASSERT()
    Currently after a firmware assert, we stop handling interrupts, so we won't run the wdog ISR,
    after 20 seconds the watchdog will trip and we will enter the bootloader.

2. a9 is still running in user mode, but for some reason the wdog task was not running.
    (e.g. a deadlock, or an infinite loop, or just high cpu usage.)
    after 10 seconds the watchdog isr will run, and we will flush the l2 cache and go into a while (1) loop,
    after another 10 seconds the watchdog will trip and we will enter the bootloader.

3. stuck inside an ISR.
    after 20 seconds the watchdog will trip and we will enter the bootloader.

4. axi bus hang.  (does accessing the OPSX still kill the AXI bus? we could try doing this with a debug command)
    after 20 seconds the watchdog will trip, but since we don't reset the axi bus we won't recover.


references
==========

Jason's documentation:

[cpu_g5_ss_eng_doc.docm](http://bby1dms01.pmc-sierra.internal/docmgmt/fileinfo.cfm?file_id=424307)

watchdog IP: Synopsys DW_apb_wdt, "Synopsys DesignWare APB Watchdog Timer Databook", 2016, PDOX_ID:423114

emails from Peter and Jason (see below)

testing
=======

# reboot the board

    /work/bby1swbuild05/nobackup/g5_board_init_env/latest/svpf_board_init_env/g5/startup/run_g5_svpf_init.sh

## start the firmware

    echo 14 | ./g5_bmp_test manual_debug digi=0

## do something to cause watchdog to trip

    ./g5_util cmd: mutex_lock

## observe first watchdog interrupt

    ./g5_util console_read digi=0 | ./g5_util annotate=stdin | more

```console
    WDOG_TASK (lock=2) owner=HOSTMSG_T TEST_A:[119] waiting (originally owned by HOSTMSG_T) spin_count == 1 old_val=1
    ~~~
    ~~~ watchdog tripped
    ~~~ flushing cache
    ~~~ cache flushed
```

## speculate that probably we're in the bootloader now...

_is there a good way to check what gets reset properly?_


registers to check ? is timer interrupt 27 going off?


## try again

    echo 14 | ./g5_bmp_test manual_debug digi=0

```console
    mscc_circular_buffer_reader: start of console log

    console_buffer_initialized
    printf is working!
    sprintf(buffer,"%0.2g",3.1415926536) = '3.1'
    malloc(20)=0x15104c50
    calling realloc(0x15104c50,200)
    hello world
    inside freertos_main_thread
    inside multitask::init()
    top_device_init: heap free 8128680
    host is little endian
    top_device_startup: heap free 7371776
    from reset to initialized takes 29313 us
    device initialization is complete!

    ... bootlog messages if packet cache hasn't been used...

    waiting for isr: heap free 6008632
    main thread stack left=57866
```

then stuck  (this was the behaviour before we also added resetting the L2 cache and GIC301)

## from another window:

    ./g5_util poll addr: 0x00d000f0 0x00d00320+16 0x00d000e4 0x00d000f8 0x1000320 0xD00000..0xD00080 0xD0C000..0xD0C004 0xD0C00C..0xD0C014 digi=0

    ./g5_util poll addr: 0x1000320 0xD0C000..0xD0C014 delay=1 digi=0

```console
    10:57:11.527 0x00d000f0 DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_INFO           BOOT_INFO              :  = 0x1003
    ...
    10:57:20.309 0x00d0033c DIGI_G5_CPU_CPU_CFG_CPU_TOP APP_SCRATCH    N=7  SCRATCH                :  = 0x00000018
    10:57:20.435 0x00d0033c DIGI_G5_CPU_CPU_CFG_CPU_TOP APP_SCRATCH    N=7  SCRATCH                :  = 0x00000019
    10:57:28.363 0x00d000f8 DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_CAUSE_REG N=0  REG_VAL 0x00000001
    10:57:28.363 0x00d000f8 DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_CAUSE_REG N=0  CLEAR_RESET_CAUSE      :  = 0         
    10:57:28.363 0x00d000f8 DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_CAUSE_REG N=0  CAUSE_SELF_RST         :  = 0         
    10:57:28.363 0x00d000f8 DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_CAUSE_REG N=0  CAUSE_HOST_RST         :  = 0         
    10:57:28.363 0x00d000f8 DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_CAUSE_REG N=0  CAUSE_WD1_TIMEOUT      :  = 0         
    10:57:28.363 0x00d000f8 DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_CAUSE_REG N=0  CAUSE_WD0_TIMEOUT      :  = 0         
    10:57:28.363 0x00d000f8 DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_CAUSE_REG N=0  CAUSE_SYS_WDT1_TIMEOUT :  = 0         
    10:57:28.363 0x00d000f8 DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_CAUSE_REG N=0  CAUSE_SYS_WDT0_TIMEOUT :  = 1         
    10:57:28.364 0x00d00350 DIGI_G5_CPU_CPU_CFG_CPU_TOP APP_SCRATCH    N=12 SCRATCH                :  = 0x00000000
    10:57:28.409 0x00d000f0 DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_INFO           BOOT_INFO              :  = 0x1003    
```

## check that pcie reenumeration works:

    /sbin/lspci -s 0000:06:00.0 -v
    echo "1" | sudo tee -a /sys/bus/pci/devices/0000:06:00.0/reset
    /sbin/lspci -s 0000:06:00.0 -v

```console
06:00.0 Unassigned class [ff00]: PMC-Sierra Inc. Device 6010 (rev 03)
    Physical Slot: 3
    Flags: bus master, fast devsel, latency 0, IRQ 48
    Memory at c0000000 (64-bit, non-prefetchable) [size=256M]
    [virtual] Expansion ROM at d0000000 [disabled] [size=128K]
    Capabilities: <access denied>
    Kernel driver in use: pci_net_host
```

## test2 again, but with MSCC_ASSERT instead of a hang.

    ./g5_util cmd: assert

## test3 again, but with a shutdown.

## test4, watchdog trip, then fw_restart:

e.g.
    echo 14 | ./g5_bmp_test manual_debug
    ./g5_util cmd: assert
    sleep 20
    # observe crash
    e.g.
    ./g5_util poll addr:  0x00d000f8
    should show:
    0x00d000f8 DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_CAUSE_REG N=0 CAUSE_SYS_WDT0_TIMEOUT :  = 1

    # recheck pcie

    /sbin/lspci -s 0000:06:00.0 -v
    echo "1" | sudo tee -a /sys/bus/pci/devices/0000:06:00.0/reset
    /sbin/lspci -s 0000:06:00.0 -v
    #
    echo 14 | ./g5_bmp_test manual_debug
    ./g5_util fw_restart



Generate a register log of the watchdog task
============================================

run the wdog unit test with the extra argument 'wdog_test' (the test hangs in a while(1);
so this test needs the special argument 'wdog_test' to work, and you need to hit ctrl-c afterwards.)

    env MSCC_L2_SYS_LOGGING=reglog.txt ./scripts/run_unit_tests.sh top_unit_test -args='--gtest_filter=wdogUnitTest.used_for_logging wdog_test' -l BUILD=eventloop

normal operation
================

@startuml

participant top
participant wdog_start #FBB4AE
participant wdog_kick #B3CDE3
participant wdog_trip #DECBE4
participant isr

top -> wdog_start

note over wdog_start,isr  #FBB4AE

<color:green>// if bootloader is entered, ensure it stays in bootloader.
00d000e4 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_CTRL BOOT_ENABLE = 0
00d000e8 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_ADDR BOOT_JMP_ADDR = 0x00000000

<color:green>// also set APP_SCRATCH 12 to 1 so that the boot loader will not re-inialize the pcie interface.
00d00350 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP APP_SCRATCH N=12 SCRATCH = 0x00000001


<color:green>// enable watchdog for a 10 second reboot
00d0c004 RMW DIGI_G5_CPU_CPU_CFG_WDOG A=0 WDT_TORR TOP = 0xf
00d0c000 RMW DIGI_G5_CPU_CPU_CFG_WDOG A=0 WDT_CR RMOD = 1
00d0c000 RMW DIGI_G5_CPU_CPU_CFG_WDOG A=0 WDT_CR WDT_EN = 1

<color:green>// also clear the GIC
00d00050 RMW DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SYS_WDT0_EN SW_RST_SYS_WDT0_EN_SCU_PERIPH_L2 = 1
00d00050 RMW DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SYS_WDT0_EN (consider coalescing) SW_RST_SYS_WDT0_EN_SYS_WDT = 3
00d00050 RMW DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SYS_WDT0_EN (consider coalescing) SW_RST_SYS_WDT0_EN_WD = 3
00d00080 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_WD_TRIG SW_RST_WD_CAPT = 0x5afec0de

00d0033c RD DIGI_G5_CPU_CPU_CFG_CPU_TOP APP_SCRATCH N=7 SCRATCH : 0x00000000
end note
... every 125 milliseconds ...
top -> wdog_kick

note over wdog_kick,isr #B3CDE3

<color:green>// writing 0x76 to the WDT_CRR register resets the watchdog counter
00d0c00c WR DIGI_G5_CPU_CPU_CFG_WDOG A=0 WDT_CRR RSVD_WDT_CRR = 0x000000
00d0c00c WR DIGI_G5_CPU_CPU_CFG_WDOG A=0 WDT_CRR WDT_CRR = 0x76
end note

... if there is a timeout ...

isr -> wdog_trip

note over wdog_trip,isr #DECBE4
<color:green>// (when WDT_STAT goes to 1)
<color:green>// 00d0c010 RDF DIGI_G5_CPU_CPU_CFG_WDOG A=0 WDT_STAT WDT_STAT : 1

<color:green>// read WDT_EOI to clear the interrupt. (not necessary)
00d0c014 RD DIGI_G5_CPU_CPU_CFG_WDOG A=0 WDT_EOI WDT_EOI : 0

<color:green>// flush cache and go into while (1); loop to wait for second interrupt
<color:green>// should we instead just cause a self reset? .. there's no point in waiting another 10 seconds.
end note

@enduml


abort procedure
===============

@startuml

participant abort_handler
participant wdog_return_to_bootloader #FBB4AE

... if there is an MSCC_ASSERT or a DATA ABORT ...

abort_handler -> wdog_return_to_bootloader

note over abort_handler,wdog_return_to_bootloader #FBB4AE

00d000e4 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_CTRL BOOT_ENABLE = 0
00d000e8 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP BOOT_ADDR BOOT_JMP_ADDR = 0x00000000

00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_SCU_PERIPH_L2 = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_DBG = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_CPU = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_DBGCPU = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_WD = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_DEVCFG = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_AMBA_IC = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_INT_AGGR = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_GDMA = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_PKTC = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_OCR = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_XVEC = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_SYS_WDT = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_UART = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_PCIE_AL = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_PCIE_IP_PWR = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_PCIE_IP_STICKY = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_PCIE_IP_NON_STICKY = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_PCIE_IP_CORE = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_PCIE_PHY = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_TIMERS = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_SPI_MC = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_JTAG2AHB = 0
00d00000 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN SW_RST_SELF_EN_TOP_NREG = 0

00d00000 RMW DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN (consider coalescing) SW_RST_SELF_EN_CPU = 3
00d00000 RMW DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN (consider coalescing) SW_RST_SELF_EN_SCU_PERIPH_L2 = 1
00d00000 RMW DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN (consider coalescing) SW_RST_SELF_EN_WD = 3
00d00000 RMW DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_EN (consider coalescing) SW_RST_SELF_EN_SYS_WDT = 3

<color:green> // note: reset happens 0x100 * 5 nanoseconds later, and the reset is held for 0x100 * 5 nanoseconds.
<color:green> // i.e. 1.28 microseconds later the reset is held for 1.28 microseconds.
00d00008 WR DIGI_G5_CPU_CPU_CFG_CPU_TOP SW_RST_SELF_TRIG SW_RST_SELF = 0x5afec0de
end note

@enduml

power complications?
====================

If the watchdog does not reset various subsystems, if there is a temperature problem, the board could overheat.

But, if the watchdog does reset various subsystems, that might cause a power spike which will glitch the axi bus and 
not permit pcie access from the host to read the error message.

Even if mscc_assert or the watchdog slowly turns various subsystems off,.. that would prevent looking at dev_cfg
to determine the cause of the problem.

emails
======

> Hi Byron, 
> 
> The GIC is not reset by those default settings.
> The A9MPcore GIC can be reset by enabling SW_RST_SYS_WDT0_EN_SCU_PERIPH_L2 - which resets the entire A9MPcore,
> both processor, snoop control unit and other peripherals (GIC, timers) and the L2 cache controller.
>
> Jason
> 
> Thanks Peter,
> 
> I was reading
> CPU_CFG.INT_AGGR.DEV_INTB_AGGR_STAT instead of
> CPU_CFG.INT_AGGR.DEV_INTB_AGGR_RSTAT
> 
> It does seem to be working, so I'll double check the a9/gic stuff,
> although the pkt_cache and timer interrupts worked without any extra code,
> so I don't know what might be wrong with how I tried to hook up the watchdog interrupt.

    From: Peter Steckhan - C33318 <Peter.Steckhan@microchip.com> 
    Sent: Wednesday, February 12, 2020 7:10 AM
    To: Byron Watt - C33511 <Byron.Watt@microchip.com>; Jason Thomson - C33298 <Jason.Thomson@microchip.com>
    Cc: Vas Panov - C51851 <Vas.Panov@microchip.com>; Grant Brydon - C33317 <Grant.Brydon@microchip.com>
    Subject: RE: watchdog help

> The interrupt should have propagated to irq[30] at the CPU, no enable required.
> Looks like reading the WDT_EOI register clears the interrupt.
> Here's the code I've got that checks that the interrupt fired:
> 
>     arm_model.regmap.CPU_CFG.INT_AGGR.DEV_INTB_AGGR_RSTAT[0].read(status, reg_val);
> 
>     if ((reg_val >> (30 + wdt_index)) & 32'h1 != 32'h1)
>       `uvm_error("WDT_INT_AGGR_CHK", "Expected the WDT int to appear at the int_aggr");
> 
>     irqs = arm_model.get_irqs(); 
>     if (irqs & (223'h3 << 30) != (223'h1 << (30 + wdt_index)))
>       `uvm_error("WDT_IRQS_CHK", "Expected the timer int to appear at the CPU IRQS");
> 
> 
>     // WDT_EOI clear the interrupt before reset is asserted
>     WDOG[wdt_index].WDT_EOI.read(status, reg_val);
>    
>     arm_model.regmap.CPU_CFG.INT_AGGR.DEV_INTB_AGGR_RSTAT[0].read(status, reg_val);
>     if ((reg_val >> (30 + wdt_index)) & 32'h1 != 32'h0)
>       `uvm_error("WDT_INT_AGGR_CHK", "Expected the WDT int to clear at the int_aggr");
> 
>     irqs = arm_model.get_irqs(); 
>     if (irqs & (223'h3 << 30) != 223'h0)
>       `uvm_error("WDT_IRQS_CHK", "Expected the timer int to clear at the CPU IRQS");


    From: Byron Watt - C33511 
    Sent: Tuesday, February 11, 2020 10:23 PM
    To: Peter Steckhan - C33318 <Peter.Steckhan@microchip.com>; Jason Thomson - C33298 <Jason.Thomson@microchip.com>
    Cc: Vas Panov - C51851 <Vas.Panov@microchip.com>; Grant Brydon - C33317 <Grant.Brydon@microchip.com>
    Subject: RE: watchdog help

> I was expecting WATCHDOG_INT_0 to fire after 10 seconds,
>
> Is there anything I have to do to enable int 30 WATCHDOG_INT_0 ?
> 
> Or should it always interrupt the cpu/pcie/...
> 
> Hmm,... I should check how to clear that interrupt too  
 
 
    From: Byron Watt - C33511
    Sent: Tuesday, February 11, 2020 4:46 PM
    To: Peter Steckhan - C33318 <Peter.Steckhan@microchip.com>; Jason Thomson - C33298 <Jason.Thomson@microchip.com>
    Cc: Vas Panov - C51851 <Vas.Panov@microchip.com>; Grant Brydon - C33317 <Grant.Brydon@microchip.com>
    Subject: RE: watchdog help
 
> Thanks Peter,... I copied and pasted your code and it seems to be working.
>
> Now I'll try to fill in the ".. bunch of stuff checking that IRQs are set correctly.."with similar freertos code.
>  
> e.g. When first interrupt happens, flush the L1/L2 cache so that debugging from host is easier.
> Figure out what blocks to reset when the second interrupt happens.
>
> (arm & uhm maybe just arm)
>  
> From: Peter Steckhan - C33318 <Peter.Steckhan@microchip.com>
> Sent: Tuesday, February 11, 2020 3:34 PM
> To: Byron Watt - C33511 <Byron.Watt@microchip.com>; Jason Thomson - C33298 <Jason.Thomson@microchip.com>
> Subject: RE: watchdog help
>  
> I'm not sure I have much interesting on the watchdog, since I assumed it worked.
> I've got some slightly gnarly re-entrant ASM I used to show that the reboot due to watchdog expiry worked,
> as well as the following SystemVerilog code that put the watchdog through some paces to show that the IRQ
> and reset toggled:
>  
>     WDOG[wdt_index].WDT_TORR.RSVD_TOP_INIT.set(1);
>     WDOG[wdt_index].WDT_TORR.TOP.set(1);
>     WDOG[wdt_index].WDT_TORR.update(status);
>     WDOG[wdt_index].WDT_CR.RMOD.set(1);
>     WDOG[wdt_index].WDT_CR.WDT_EN.set(1);
>     WDOG[wdt_index].WDT_CR.update(status);
>     WDOG[wdt_index].WDT_CRR.WDT_CRR.write(status, 8'h76);
>  
>     wdt_expiry = $time();
>  
>     // Poll for the interrupt to occur
>     `uvm_info("DEBUG", "Poll for the interrupt to occur", UVM_NONE);
>     WDOG[wdt_index].WDT_STAT.read(status, reg_val);
>     while (reg_val != 32'h1)
>       WDOG[wdt_index].WDT_STAT.read(status, reg_val);
>  
>     wdt_expiry = $time() - wdt_expiry;
>  
> .. bunch of stuff checking that IRQs are set correctly..
>  
>     // Poll for the interrupt to re-occur
>     `uvm_info("DEBUG", "Poll for the interrupt to re-occur", UVM_NONE);
>     WDOG[wdt_index].WDT_STAT.read(status, reg_val);
>     while (reg_val != 32'h1)
>       WDOG[wdt_index].WDT_STAT.read(status, reg_val);
>  
>     // Wait for reset to be asserted by the WDOG
>     `uvm_info("DEBUG", "Wait for reset to be asserted by the WDOG", UVM_NONE);
>     while (arm_model.get_cpu_async_resetb() != ~(2'h1 << wdt_index))
>         @(posedge m_env.dev_cfg_axi_env.vif.aclk);
>  
>     `uvm_info("DEBUG", "Wait for reset to be de-asserted by the reset blk", UVM_NONE);
>     while (arm_model.get_cpu_async_resetb() != 2'h3)
>         @(posedge m_env.dev_cfg_axi_env.vif.aclk);
>  
>     // Reset will actually disable the WDT because disable is the default...
 
 
    From: Byron Watt - C33511
    Sent: Tuesday, February 11, 2020 5:13 PM
    To: Jason Thomson - C33298 <Jason.Thomson@microchip.com>; Peter Steckhan - C33318 <Peter.Steckhan@microchip.com>
    Subject: watchdog help
 
> For some reason I thought the watchdog was the same on g4 as it was in g5,... but it looks a little different,...
> do you have any cheat sheet that I can use to figure out how to program it?
>
> So far I have:
>
> WDT_CRR = 0x76;
>
> But I assume you've already done the hard work of how to test it,... can I copy/paste code from you ?
>  
> 1.       Synopsys DW_apb_wdt, "Synopsys DesignWare APB Watchdog Timer Databook", 2016,PDOX_ID:423114
>
> 1.1.1       System Watchdog Features
> * Configurable 32-bit wide countdown timer.
> * Can be configured to interrupt after a first timeout and output a reset pulse if the interrupt is not cleared prior to a second timeout, or simply output a reset pulse upon first timeout.
> * Programmable timeout range (period) selected from 16 predefined values ranging in increments of powers of 2 from 216-1 to 231-1 (327us to 10.7s range with 200MHz clock).
> * Programmable reset pulse length.
>  
> System Watchdog Functional Description
>
> The subsystem integrates two hardware system watchdog timers. The watchdog counts from a configurable timeout
> value in descending order to zero.  If the times out, an interrupt is signaled to the ARM core. Writing 0x76
> to the watchdog Counter Restart register at any time restarts the counter to the new value and clears a pending
> interrupt.  If the watchdog times out a second time prior to the interrupt being cleared, then a reset output will
> become asserted. The watchdog can alternately be configured to issue a reset upon the initial timeout instead of
> first issuing an interrupt.  The Watchdog module intention is to apply a system reset in the event of a software
> failure, providing a way to recover from software crashes.
