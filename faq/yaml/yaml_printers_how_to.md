how to generate a yaml print file
==================================

If you need to re-generate an existing yaml class_print.c or member_print.c file use:

    make rebuild_printers

To generated yaml class_print and member_print files for a new class use the following steps:

  1. add the class .h file to /scripts/doxygen/doxygen_api_xml_config.txt

  2. create two new files in the /device/generated/src/ folder. 
        Example:

        touch device/generated/src/nfic_task_class_print.c
        touch device/generated/src/nfic_task_member_print.c

  3. if the class header file contains "enum" structures you will need to create a printers.h file.
     To create a printers.h file use

        make device/generated/inc/nfic_task_printers.h REBUILD_PRINTERS=TRUE

    This will output

        generating print functions for device/blk/nfic_task/inc/nfic_task.h
        creating device/generated/src/nfic_task_printers.c
        creating device/generated/inc/nfic_task_printers.h

  4. Next, add nfic_task_printers.h to device/common/inc/g5_yaml_printer_impl.h

        #include "nfic_task_printers.h"

  5. Generate the yaml printer files

        make rebuild_printers

  6. Done!
