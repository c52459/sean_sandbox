smoke testing db_restore
========================

there isn't a c example for db_restore,... but don't be afraid of python.

Shakil says that python is fun, quick and easy.

As a quick check that db_restore is working use:

    cd <path to workspace>
    make flab

Grab a board, (bsub -Ip -q normal -R g5 -app g5 -R revb terminal)

    cd <path to g5env>
    svn up *
    cd g5/startup
    source setup_env
    ./download_g5sdk.py -mode embedded -ul <path to workspace>
    python3.6 -u -m pytest ../../g5tests/sw/testcases/enet/test_enet_gfp_oduflex_otucn.py --testrunner_mode DB_RESTORE --verbosity=WARN --regression_mode --skip_svpf_init -k 'test_enet_gfp_oduflex_otucn_muxponder[0-20xGE10_OTUC2_NRZ]' 

wait for

<span style="color:green">PASSED</span>

<span style="color:green">============== 1 passed, 1 deselected in 84.97 seconds ==============</span>

see,... nothing to be afraid of.


_remember to exit the terminal to release the board back to lsf!_