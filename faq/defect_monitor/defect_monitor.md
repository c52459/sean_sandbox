g5_util defect_monitor
======================

when debugging 2lab firmware, we would like to know when a defect happens as we are stepping through firmware with gdb.

we have a 2 step process to do this:

# capture list of defects and save it to a file (requires firmware running)

```plantuml
@startuml
skinparam ParticipantPadding 40
skinparam BoxPadding 40
'--------- participants -------------
box "g5_util" #moccasin
  actor "defects_monitor_save_to_file" as g5_util
endbox
box "firmware" #lightblue
  participant firmware
endbox
box "defects.yaml" #lightgreen
  participant defects.yaml
endbox
'--------- call sequence diagram -------------
g5_util -> firmware : read defects and defect "tuples"
g5_util -> defects.yaml
hide footbox
@enduml
```

# read list of defects from file and monitor defects without interacting with firmware

```plantuml
@startuml
skinparam ParticipantPadding 40
skinparam BoxPadding 40
'--------- participants -------------

box "g5_util" #moccasin
  actor "defects_monitor_read_from_file" as g5_util
  participant "monitor" as monitor
endbox
box "direct\nregister\naccess" #cyan
  database "G5 registers" as registers
endbox
box "defects.yaml" #lightgreen
  participant defects.yaml
endbox
'--------- call sequence diagram -------------
g5_util -> defects.yaml : read defects from file
g5_util -> monitor : grab defects
monitor -> registers : latch RFRMS
monitor -> registers : read _I and _V registers
monitor -> registers : clear _I registers
note over g5_util, monitor
  repeat 100 times and display results.
end note
note over g5_util, monitor
  monitor and display new defects in red
end note

hide footbox
@enduml
```

here is a way to do it:

## get a comexpress

    bsub -Ip -q normal -R g5 -R revb -app g5 terminal

## start three windows

## window 1 - run the device

    launch the device with gdb (this also works with flab, but presumably you want to single step through a function to find out when something breaks.)

    gdb -ex 'b g5_dev_module_switch::handle_disconnect' -ex 'b cpb::chnl_deprov' -ex r ./device

## window 2 - run the host code

    start the example and let it run up to the point that you want to start debugging.

    gdb -ex 'b g5_api_switch_disconnect' -ex r --args g5_100ge_ilkn_pkt_cfg nodownload notimeout

## window 3 - g5_util defect_monitor_save_to_file=defects.yaml

    capture the defect registers to a text file. (requires that the device is running (not sitting in a breakpoint.))

    ./g5_util defect_monitor_save_to_file=defects.yaml

you now have a file that you can use to monitor defects from the host.

continue doing whatever you'd like to do.

maybe even restart the host and the device.

you can edit the file defects.yaml and remove any defects you don't care about.

when you are ready to start stepping through the device code first launch the host based defects_monitor_read_from_file:

## window 3 - g5_util defect_monitor_read_from_file=defects.yaml

    ./g5_util defect_monitor_read_from_file=defects.yaml

This displays the current defects just as if you had asked firmware 100 times:

```console
    17:23:40.954 enet_port[0x2000].phy.rx_cfc.fifo_empty:  V=0; (21 of last 100); overall: 21.000%
    17:23:40.954 enet_port[0x2000].phy.rx_cfc.fifo_full:  V=0
    17:23:40.954 enet_port[0x2000].mac.tx_jabber_pkt:  V=0 I=0
    17:23:40.954 enet_port[0x2000].mac.rx_lfault:  V=1 I=1

    defect_collect: n=100, total=50.13 milliseconds, min=493.0, max=549.0, avg=501.3 usec
    656 defects; 159 registers; 0 RFRMs; 170 active defects; 0 ignored defects

    (r) restart (t) toggle display of register names (q) quit
    --> monitoring all defects (will display any new defects that appear)
```

note: defects that have always been 0 are shown in grey.
defects that are always 0 or 1 do not have any extended stats shown.
if a defect is sometimes 0 and sometimes 1, then it shows how often it has been 1 in the last 1000 reads, and overall how often it has been 1

the defect_collect: line shows the number of collections that have been performed, and how long it took to latch the RFRMs and read all the defect registers.

AT THIS POINT, NEW DEFECTS WILL BE SHOWN IN RED

Note: these "ic_ramp_done" defects are a very infrequent, and I assume harmless:

```console
    17:26:04.542 top_defects.csu_fracn_serdes.port[28].ic_ramp_done:  V=0 I=1; (1 of last 1000); overall:  0.001%
    defect_collect: n=284933, total=2.382 minutes, min=491.0, max=2630, avg=501.6 usec
```

If a defect happens while you are stepping through your code, it will be displayed in red in the g5_util window.

To test this with g5_bmp_test you could use: ./g5_util set: DIGI_G5_SAR_SAR_OBRC_OBRC.SW_RESET.TSB_SW_RST=1

and observe that some sar defects that appear are displayed in red.

    11:28:52.539 ilkn_cbr[0x3000].sar_capture.mpma_dsp.int_thres_unf:  I=1; (1 of last 1000); overall: 00.009%
    11:28:52.539 ilkn_cbr[0x3000].sar_recovery.ddc_dst.cfc.fifo_udr:  I=1; (1 of last 1000); overall: 00.009%

Be sure to halt g5_util defect_monitor_read_from_file if you are restarting the device since it will be monitoring registers that get powered off, and that might lead to an AXI bus hang which will lead to a PCIE bus hang which requires a power cycle of the comexpress to recover.

# new feature select just a single channel id

As requested by Conrad, you can limit the defects that are monitored without editing the file by using the command line argument "defect_filters:" 

You can add a list of strings to limit the defects to just the defects that contain the specified string:

    ./g5_util defect_monitor_read_from_file=defects.yaml defect_filters: 0x2000 0x1000

e.g. the above command would only show defects with 0x2000 and 0x1000 in the defect name.

you can also add a '-' to the filter to exclude defects that contain the exclusion string. e.g.

    ./g5_util defect_monitor_read_from_file=defects.yaml defect_filters: 0x2000 0x1000 -fifo_empty

means select only channels 0x2000 and 0x1000 but exclude the fifo_empty defects.

note: if you have a channel 0x1000 and a channel 0x10000, then 0x1000 would match both because the match is just a 'strstr' command, but you could include the '[]' to avoid that e.g.

    ./g5_util defect_monitor_read_from_file=defects.yaml defect_filters: '[0x2000]' '[0x1000]' -fifo_empty

