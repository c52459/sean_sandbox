defect monitor code walkthrough
===============================

the defects monitor is a two step process,... first you need to capture the current defects and save it to a file.

then whenever you want you can read the list defects from the file and monitor those defects from the host.

# capture list of defects and save it to a file (requires firmware running)

@startuml
skinparam ParticipantPadding 40
skinparam BoxPadding 40
'--------- participants -------------
box "g5_util" #moccasin
  actor "defects_monitor_save_to_file" as g5_util
endbox
box "firmware" #lightblue
  participant firmware
endbox
box "defects.yaml" #lightgreen
  participant defects.yaml
endbox
'--------- call sequence diagram -------------
g5_util -> firmware : get bound defects and their register addresses
g5_util -> defects.yaml : save list of defects to file
hide footbox
@enduml

# read list of defects from file and monitor defects without interacting with firmware

@startuml
skinparam ParticipantPadding 40
skinparam BoxPadding 40
'--------- participants -------------

box "g5_util" #moccasin
  actor "defects_monitor_read_from_file" as g5_util
  participant "monitor" as monitor
endbox
box "direct\nregister\naccess" #cyan
  database "G5 registers" as registers
endbox
box "defects.yaml" #lightgreen
  participant defects.yaml
endbox
'--------- call sequence diagram -------------
g5_util -> defects.yaml : read defects from file
g5_util -> monitor : monitor_defects()
monitor -> registers : latch RFRMS
monitor -> registers : read _I and _V registers
monitor -> registers : clear _I registers
note over g5_util, monitor
  repeat 100 times and display results.
end note
note over g5_util, monitor
  monitor and display new defects in red
end note

hide footbox
@enduml


# the interesting part of the code

The interesting part of saving the list of defects to a file is that we need to somehow build a flat list of defects.

@startuml
skinparam ParticipantPadding 40
skinparam BoxPadding 40
'--------- participants -------------
box "g5_util" #moccasin
  actor "defects_monitor_save_to_file" as g5_util
  participant "discover_defects" as discover_defects
endbox
box "firmware" #lightblue
  participant firmware
endbox
box "defects.yaml" #lightgreen
  participant defects.yaml
endbox
'--------- call sequence diagram -------------
g5_util -> firmware : g5_api_defect_polled_transfer_start()
g5_util -> firmware : resource_mapping = g5_api_defect_chnl_ids_get()
g5_util -> discover_defects : discover_defects()
note over discover_defects, firmware
  get the register address associated with each defect.
end note
discover_defects ->  firmware : call g5_api_defect_chnl_tuple_get()\nfor each channel id in resource_mapping
note over discover_defects
  build a flat list of defects
  <color:red>this calls defects::traverse()
  <color:red>which is a 'visitor' pattern.
  <color:red>described here...
end note
discover_defects -> discover_defects : call defect_structure->traverse()\nfor each channel id in resource_mapping\nto assemble a flat list of defects
g5_util -> defects.yaml : save defects to file
hide footbox
@enduml

interesting readings related to the visitor pattern:
[stop reimplementing the virtual table and start using double dispatch](https://gieseanw.wordpress.com/2018/12/29/stop-reimplementing-the-virtual-table-and-start-using-double-dispatch/)

The interesting part of the code is the 'traverse' function that was added to each defect structure.

You pass in a visitor object to the traverse function, and the visitor object's 'visit' method is called once for each defect in the defect structure.

@startuml
digraph G {
    rankdir=TD;
    graph [fontsize="16"]
    node [shape="box"]
    /* --- nodes --- */
    g5_defect_device_resource_mapping_t ;
    otn_port_to_channel_id [label=<otn_port[]<br/>
    <font color="blue">maps index to channel_id</font>>] ;
    g5_defect_device_defects_t ;
    otn_port [label="g5_defect_otn_port_segment_t otn_port[0]"] ;
    port[label="g5_defect_dsis_serdes_port_t port"] ;
    rx_los [label="g5_defect_ive_t rx_los"] ;
    somethingelse [label="g5_defect_ive_t somethingelse"] ;
    node [shape="oval"]
    visitor [label="visitor"] ;
    visitor2 [label="visitor"] ;
    /* --- edges --- */
    g5_defect_device_resource_mapping_t -> otn_port_to_channel_id 
    g5_defect_device_defects_t -> otn_port [ label=<  traverse(<font color="green">visitor.path="otn_port[0x2000]"</font>  )> ]
    otn_port -> port [ label=<  traverse(<font color="green">visitor.path="otn_port[0x2000].port"</font>  )> ]
    port-> rx_los [ label=<  traverse(<font color="green">visitor.path="otn_port[0x2000].port.rx_los"</font>  )> ]
    rx_los -> visitor [ label=<  visit(<font color="green">visitor.path="otn_port[0x2000].port.rx_los"</font>  )> ]
    port-> somethingelse [ label=<  traverse(<font color="green">visitor.path="otn_port[0x2000].port.somethingelse"</font>  )> ]
    somethingelse -> visitor2 [ label=<  visit(<font color="green">visitor.path="otn_port[0x2000].port.somethingelse"</font>  )> ]
}
@enduml

This is similar to the recursive yaml print, with a slight modification to how decending one level is handled.  In the yaml_print functions, each structure adds 1 to the current depth as it decends and subtracts one as it returns.

I changed this slightly to create a new 'child' visitor as it decends one level, so that each node has a new instantiation of the visitor so that you can append the full path of the defect name.

Here's an example of the traverse function from mscc_defect_otn_debug_struct_printers.c

~~~cpp
void g5_defect_odu_tfrm_chnl_t::traverse(g5_defect_visitor *visitor, g5_defect_tuple_t *defect_tuple)
{
    if (summary.is_valid)
    {
        iae.traverse(g5_defect_visitor_child(visitor,"iae"),
                     &defect_tuple[offsetof(g5_defect_odu_tfrm_chnl_t,iae)]);
        
        cfc_fifo_ovrflow.traverse(g5_defect_visitor_child(visitor,"cfc_fifo_ovrflow"),
                                  &defect_tuple[offsetof(g5_defect_odu_tfrm_chnl_t,cfc_fifo_ovrflow)]);
    }
}
~~~

This is the base cass of a visitor, you extend this class g5_defect_visitor and put whatever fun things you want in your own version of the 'visit' method:

~~~cpp
class g5_defect_visitor {
public:
    /// child create a child node
    virtual g5_defect_visitor *child_create(const char *name_ptr) = 0;
    /// visit defect function
    virtual void visit(g5_defect_ive_t *defect_ive,g5_defect_tuple_t *defect_tuple) = 0;
} ;
~~~

Here is an example of the derived class debug_print_visitor that is used to print each defect:

~~~cpp
/*
 * class used to 'traverse' a defect struct to visit each defect and print them similar to debug_chnl_print
 *
 * debug_print_visitor::visit(g5_defect_ive_t,g5_defect_tuple_t) is called once for every defect.
 *
 * Instead of displaying in yaml format, it displays one line per defect with the full path.
 * e.g. mapotn[0x2000].stg4.odu_rfrm.pm.dias: {V: 1, I: 0, E: 0}
 */
class debug_print_visitor : public g5_defect_visitor {
public:
    /// top level visitor constructor
    debug_print_visitor(const char *name_ptr, g5_defect_print_filter_t _print_sel);
    /// child visitor constructor
    debug_print_visitor(debug_print_visitor *parent,const char *name_ptr);
    
    virtual g5_defect_visitor *child_create(const char *name_ptr);

    virtual void visit(g5_defect_ive_t *defect_ive,g5_defect_tuple_t *defect_tuple);
protected:
    /// how to print
    g5_defect_print_filter_t print_sel;
    /// e.g. mapotn[0x2000].stg4.odu_rfrm.pm.dias: {V: 1, I: 0, E: 0}
    std::string defect_path;
} ;
~~~

here's the implementation of the constructor, note: it concantenates the child name with the parent name.

~~~cpp
/*
 * child visitor constructor
 *
 * @param[in] parent  - parent visitor
 * @param[in] name_ptr - name of this child.
 */
debug_print_visitor::debug_print_visitor(debug_print_visitor *parent,const char *name_ptr)
    : print_sel(parent->print_sel)
{
    if (parent->defect_path.empty())
    {
        defect_path = name_ptr;
    }
    else
    {
        // update the defect path to be parent "." child
        // e.g. if parent is "mapotn[0x2000]" and name_ptr is "bmp_imp"
        // then the new defect_path is "mapotn[0x2000].bmp_imp"
        defect_path = parent->defect_path + "." + name_ptr;
    }
}
~~~

And here's the 'visit' code:

~~~cpp
/// called once for each defect
void debug_print_visitor::visit(g5_defect_ive_t *defect_ive,g5_defect_tuple_t *defect_tuple)
{
    BOOL8 print_it;

    switch (print_sel)
    {
        case G5_DEFECT_PRINT_FILTER_I_ASSERTED:
            print_it = defect_ive->i;
            break;

        case G5_DEFECT_PRINT_FILTER_V_ASSERTED:
            print_it = defect_ive->v;
            break;

        case G5_DEFECT_PRINT_FILTER_V_OR_I_ASSERTED:
            print_it = defect_ive->v || defect_ive->i;
            break;

        default:
            print_it = TRUE;
            break;
    }

    if (print_it)
    {
        // prints something like mapotn[0x2000].bmp_imp.ais {V: 0, I: 1, E: 0}
        MSCC_PRINT("%s: {V: %d, I: %d, E: %d}\n", defect_path.c_str(), defect_ive->v, defect_ive->i, defect_ive->e);
    }
}
~~~

This is a virtual method called whenever we decend one level:

~~~cpp
/// create a child for each node decended.
g5_defect_visitor *debug_print_visitor::child_create(const char *name_ptr)
{
    debug_print_visitor *child = new debug_print_visitor(this,name_ptr);
    return child;
}
~~~

Did anyone spot the sneaky bit?

~~~cpp
        iae.traverse(g5_defect_visitor_child(visitor,"iae"),
                     &defect_tuple[offsetof(g5_defect_odu_tfrm_chnl_t,iae)]);

~~~

When we decend, we want to create another instance of the class debug_print_visitor.

But we only know it's base class g5_defect_visitor.  There is no way to instantiate a new derived class in c++.  i.e. a constructor cannot be virtual.

So instead what we need to do is use a virtual function to create the child like:

~~~cpp
g5_defect_visitor *new_defect_visitor = defect_visitor->child_create("iae");
iae.traverse(new_defect_visitor);
delete new_defect_visitor;
~~~

Unfortunately we need the 'new' which happens in child_create(),... It would have been nice if it was an anonymous object instantiated on the stack, but we can't so it needs to be instantiated on the heap.

At least we can create an anonymous object to replace the new, use, delete:

We use an anonymous class g5_defect_visitor_child, which calls g5_defect_visitor::child_create() in the constructor, the delete is done in the destructor, and an implicit conversion function is defined to convert a g5_defect_visitor_child to a pointer to a g5_defect_visitor.  The child visitor object is still created on the heap, but at least the new and delete is managed for us without having to worry about memory leaks.

~~~cpp
class g5_defect_visitor_child {
public:
    /// constructor
    g5_defect_visitor_child(g5_defect_visitor *_parent,const char *name_ptr)
    {
        child = _parent->child_create(name_ptr);
    }
    /// destructor
    ~g5_defect_visitor_child() {
        delete child;
    }
    /// implicit conversion to a defect visitor
    operator g5_defect_visitor *() const { return child; }

private:
    /// child visitor (one child is created for each node tranversed so that the path can be remembered)
    g5_defect_visitor *child;
} ;
~~~

So what this means is that this code:

~~~cpp
g5_defect_visitor *new_defect_visitor = defect_visitor->child_create("iae");
iae.traverse(new_defect_visitor);
delete new_defect_visitor;
~~~

Is the same as:

~~~cpp
iae.traverse(g5_defect_visitor_child(visitor,"iae"));
~~~


# another fun bit of code: keeping track of the last 1000 samples

class defect_stats is used to track how many 0's and 1's have happened over all.

e.g. defects_stats::print_stats() shows the current value, and how many 1's there have been in the last 1000 samples and what percentage overall it has been 1.

    V=0; (21 of last 100); overall: 21.000%

A circular buffer of the last 1000 0's and 1's is a perfect fit for std::bitset

This defines an array of 1000 bits:
~~~cpp
std::bitset<1000> history;
~~~
This updates the value in a circular buffer:
~~~cpp
UINT32 index = num_samples%1000;
history[index] = value;
~~~
And this counts the number of bits set in the history window:
~~~cpp
history.count()
~~~
note: std::bitset<>::count uses __builtin_popcount() so only a few instructions to count the number of 1's set,.. although i suppose you could maintain a count_of_last_1000, and simply increment or decrement it each time you update the history,... but any excuse to use __builtin_popcount() is fun.

# important files for code review:

relevant files for this feature:
- - -

    faq/defect_monitor/defect_monitor.md
    faq/defect_monitor/defect_monitor_code_walkthrough.md

usage/design documentation

- - -

    host/g5_api/inc/g5_api_defect_print.h
    host/g5_api/inc/g5_api_defect_to_python.h
    host/g5_api/src/g5_api_defect_print.c
    host/g5_api/src/g5_api_defect_to_python.c

added the 'traverse' function to each defect structure and this holds the definition of the base class g5_defect_visitor

- - -

    host/shared_api_host_device/inc/mscc_defect_common.h
    host/shared_api_host_device/inc/mscc_defect_enet.h
    host/shared_api_host_device/inc/mscc_defect_ilkn.h
    host/shared_api_host_device/inc/mscc_defect_mapotn.h
    host/shared_api_host_device/inc/mscc_defect_otn.h
    host/shared_api_host_device/inc/mscc_defect_top.h
    host/shared_api_host_device/inc/mscc_defect_types.h

added the 'traverse' function to each defect structure

- - -

    scripts/generate_debug_struct_printers.pl

generate_debug_struct_printers.pl modified to also generate the traverse() function

- - -

    host/shared_host_device/generated/inc/mscc_defect_common_debug_struct_printers.h
    host/shared_host_device/generated/inc/mscc_defect_enet_debug_struct_printers.h
    host/shared_host_device/generated/inc/mscc_defect_ilkn_debug_struct_printers.h
    host/shared_host_device/generated/inc/mscc_defect_mapotn_debug_struct_printers.h
    host/shared_host_device/generated/inc/mscc_defect_otn_debug_struct_printers.h
    host/shared_host_device/generated/inc/mscc_defect_top_debug_struct_printers.h
    host/shared_host_device/generated/src/mscc_defect_common_debug_struct_printers.c
    host/shared_host_device/generated/src/mscc_defect_enet_debug_struct_printers.c
    host/shared_host_device/generated/src/mscc_defect_ilkn_debug_struct_printers.c
    host/shared_host_device/generated/src/mscc_defect_mapotn_debug_struct_printers.c
    host/shared_host_device/generated/src/mscc_defect_otn_debug_struct_printers.c
    host/shared_host_device/generated/src/mscc_defect_top_debug_struct_printers.c

new generated 'traverse' function for each defect structure (also fixed the generation of the 'mscc_defect_top.h' file)

- - -

    tests/g5_util/src/g5_util_defects.c

trivial changes

- - -

    tests/g5_util/src/g5_util_defect_monitor.c
    tests/g5_util/src/g5_util_defect_monitor.h

the guts of the changes are in g5_util_defect_monitor

- - -

    tests/g5_util/src/g5_util_defect_monitor_read_from_file.c

this uses libyaml to parse the defects.yaml file that was saved.

- - -

    tests/g5_util/src/g5_util_defect_segment_monitors.c

this implements:

TestUtil::discover_defects()

and the class defect_enumerator

g5_util_defect_segment_monitors evolved a lot,... now it should be renamed to just 'discover' defects.
it implements the defect_enumerator class so we should rename it defect_enumerator.

- - -

    tests.mk
    tests/libyaml
    tests/libyaml/inc
    tests/libyaml/inc/yaml.h
    tests/libyaml/src
    tests/libyaml/src/License
    tests/libyaml/src/libyaml_api.c
    tests/libyaml/src/libyaml_dumper.c
    tests/libyaml/src/libyaml_emitter.c
    tests/libyaml/src/libyaml_loader.c
    tests/libyaml/src/libyaml_parser.c
    tests/libyaml/src/libyaml_reader.c
    tests/libyaml/src/libyaml_scanner.c
    tests/libyaml/src/libyaml_writer.c
    tests/libyaml/src/yaml_private.h

added open source libyaml so that we can parse a yaml file.

- - -