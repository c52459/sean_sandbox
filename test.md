With old freertos labs it didn't work:


```plantuml
@startuml
skinparam ParticipantPadding 40
skinparam BoxPadding 40
'--------- participants -------------
box "g5_util" #moccasin
  actor "defects_monitor_save_to_file" as g5_util
endbox
box "firmware" #lightblue
  participant firmware
endbox
box "defects.yaml" #lightgreen
  participant defects.yaml
endbox
'--------- call sequence diagram -------------
g5_util -> firmware : read defects and defect "tuples"
g5_util -> defects.yaml
hide footbox
@enduml
```


```plantuml
@startuml
actor hostmsg
actor "thread_a\nhigh priority" as high_priority_thread
actor "thread_b\nlow priority" as low_priority_thread
control gdma
activate hostmsg #FBB4AE
hostmsg -> gdma ++ #FBB4AE : acquire ch 1
return ch1 granted
hostmsg -> gdma ++ #FBB4AE : acquire ch 2
return ch2 granted
hostmsg -> high_priority_thread ++ #FED9A6 : start thread a (high priority)
hostmsg -> low_priority_thread ++ #CCEBC5 : start thread b (low priority)
hostmsg -> hostmsg : sleep a smidge
high_priority_thread -> gdma ++ #FED9A6 : acquire ch 1
low_priority_thread -> gdma ++ #CCEBC5 : acquire ch 2
hostmsg -> gdma : release ch 2
gdma -> high_priority_thread : retest if ch 1 is available (not)
gdma -> high_priority_thread : retest if ch 1 is available (not)
... low priority thread never awakened to retest if ch2 available ...
hostmsg -> low_priority_thread : wait for low priority thread to finish
hostmsg -> gdma : release ch 1
hostmsg -> hostmsg : wait for high priority thread to finish
hide footbox
```

With current code it should work properly:

```plantuml
@startuml
actor hostmsg
actor "thread_a\nhigh priority" as high_priority_thread
actor "thread_b\nlow priority" as low_priority_thread
control gdma
activate hostmsg #FBB4AE
hostmsg -> gdma ++ #FBB4AE : acquire ch 1
return ch1 granted
hostmsg -> gdma ++ #FBB4AE : acquire ch 2
return ch2 granted
hostmsg -> high_priority_thread ++ #FED9A6 : start thread a (high priority)
hostmsg -> low_priority_thread ++ #CCEBC5 : start thread b (low priority)
hostmsg -> hostmsg : sleep a smidge
high_priority_thread -> gdma ++ #FED9A6 : acquire ch 1
low_priority_thread -> gdma ++ #CCEBC5 : acquire ch 2
hostmsg -> gdma : release ch 2
gdma -> high_priority_thread : retest if ch 1 is available (not)
gdma -> low_priority_thread -- : retest if ch 2 is available (yes!)
hostmsg -> low_priority_thread : wait for low priority thread to finish
low_priority_thread -> hostmsg -- : finished
hostmsg -> gdma : release ch 1
gdma -> high_priority_thread -- : retest if ch 1 is available (yes!)
hostmsg -> high_priority_thread : wait for high priority thread to finish
high_priority_thread -> hostmsg -- : finished
hide footbox
```

